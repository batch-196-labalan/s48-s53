import Swal from 'sweetalert2';
import {Navigate, Link} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Login(){


//Allows us to consume the User context object and its properties to use for user validation. Syntax: useContext(provider)
const{user, setUser} = useContext(UserContext);
console.log(user);	


const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [isActive, setIsActive] = useState(false);

console.log(email);
console.log(password);


//Form onSubmit
function authenticate(e){
	e.preventDefault();



	fetch('http://localhost:4000/users/login', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password:password
		})
	})
	.then(res => res.json())

	.then(data => {
		console.log(data); //result : access token

		if(typeof data.accessToken !== "undefined"){
			localStorage.setItem('token', data.accessToken) //remove from email line 51
			retrieveUserDetails(data.accessToken); //from line 75 (invoked)


		//package from sweetalert2
			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to Booking App of 196!"
			});
		} else {

			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your credentials"
			});
		};
	});


//set the email of the authenticated user in the local storage
//syntax: localStorage.setItem('propertyName/key', value);s
	// localStorage.setItem("email", email);



//Access user info through local storage to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout.

//when states change components are rerendered and the AppNavbar component will be updated based on the credentials:

// setUser({
// 	email: localStorage.getItem('email')
// });



//clear input fields
	setEmail('');
	setPassword('');

	// alert(`${email} Login successfully`);
};


//Get/retrieve user details
const retrieveUserDetails = (token) => {

	fetch('http://localhost:4000/users/getUserDetails', {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		setUser({
			id: data._id,
			isAdmin: data.isAdmin
		})
	})
};



useEffect(() => {

	if(email !== '' && password !== ''){

		setIsActive(true);

	} else {
		setIsActive(false);
	}

}, [email, password]);


	return(

		(user.id !== null) ?
		<Navigate to="/courses"/>

		:

		// (user.email !== null) ?
		// <Navigate to="/courses"/>

		// :

		<>
		<h1>Login:</h1>
		<Form onSubmit={e => authenticate(e)}>
			<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value= {email}
				onChange= {e => setEmail(e.target.value)}
			/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
		   </Form.Group>

		   <Form.Group controlId="password">
		   <Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value= {password}
				onChange= {e => setPassword(e.target.value)}
			/>
		   </Form.Group>



		   <p>Not yet registered? <Link to="/register">Register Here</Link></p>


		  { isActive ?

		  	<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Login</Button>

		  	:

		  	<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Login</Button>


		  }


		</Form>
		</>
	)
}

