import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// localStorage.clear();

	const {unsetUser, setUser}= useContext(UserContext);

	unsetUser();

	//Placing the "setUser" func inside of a useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different components

	//by adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user
	useEffect(() => {

		setUser({id: null }) //setUser({email: null })
	});

	return(
		<Navigate to="/login"/>

	)
};