import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Form, Button} from 'react-bootstrap';

export default function Register(){

//
const{user} = useContext(UserContext);
const history = useNavigate();

// console.log(user);	


const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState ('');
const [mobileNo, setMobileNo] = useState('');
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
// const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);

// console.log(email);
// console.log(password1);
// console.log(password2);


//Form onSubmit
function registerUser(e){
	e.preventDefault();

	fetch('http://localhost:4000/users/checkEmailExists',{
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email
		})
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data);


		if(data){
			Swal.fire({
				title: "Duplicate email found",
				icon: "info",
				text: "The email that you're trying to register already exist"
			});
		} else {
			fetch('http://localhost:4000/users',{
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password: password
				})

			})
			.then(res => res.json())
			.then(data =>{
				console.log(data);

				if(data.email){
					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Thank you for registering"
					});
					history('/login');
				} else {
					Swal.fire({
						title: "Registration failed",
						icon: "error",
						text: "Something went wrong, try again"
					})
				}
			})

		};
	});


	//clear input fields
	setFirstName('');
	setLastName('');
	setMobileNo('');
	setEmail('');
	setPassword('');


	// alert('Thank you for registering');
};



useEffect(() => {

	if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo.length === 11){

		setIsActive(true);

	} else {
		setIsActive(false);
	}

}, [email, firstName, lastName, password, mobileNo]);


	return(

		(user.id !== null)? //(user.email !== null)?
		<Navigate to="/courses"/>
		
		:
		<>
		<h1>Register here:</h1>


		 <Form.Group controlId="firstName">
		   <Form.Label>First Name</Form.Label>
			<Form.Control
				type="text"
				placeholder="Enter your first name here"
				required
				value= {firstName}
				onChange= {e => setFirstName(e.target.value)}
			/>
		   </Form.Group>


		    <Form.Group controlId="lastName">
		   <Form.Label>Last Name</Form.Label>
			<Form.Control
				type="text"
				placeholder="Enter your last name here"
				required
				value= {lastName}
				onChange= {e => setLastName(e.target.value)}
			/>
		   </Form.Group>


		   <Form.Group controlId="mobileNo">
		   <Form.Label>Mobile No</Form.Label>
			<Form.Control
				type="text"
				placeholder="Enter your mobile no here"
				required
				value= {mobileNo}
				onChange= {e => setMobileNo(e.target.value)}
			/>
		   </Form.Group>


		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value= {email}
				onChange= {e => setEmail(e.target.value)}
			/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>
		   </Form.Group>

		   <Form.Group controlId="password">
		   <Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value= {password}
				onChange= {e => setPassword(e.target.value)}
			/>
		   </Form.Group>

		  


		  { isActive ?

		  	<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register</Button>

		  	:

		  	<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Register</Button>


		  }


		</Form>
		</>
	)
}

