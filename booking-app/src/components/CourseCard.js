import {useState} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// console.log(courseProp);
	// console.log(typeof courseProp);//object


	//Object destructuring
	const {name, description, price, 
		_id} = courseProp;


	// react hooks - useState  > store its state
	// const [count, setCount] = useState(0);
	// console.log(useState(0));

	// function enroll(){
	// 	setCount(count + 1);
	// 	console.log(`Enrollees: ${count}`);
	// };


	// const [count, setCount] = useState(10);
	// console.log(useState(10));

	// const [enroll, setEnroll] = useState(0);

	// function seat(){
		
	// 	if (count === 0){
	// 		alert ("No more seats available, check back later")
			
	// 	} else {
	// 		setCount(count - 1);
	// 		setEnroll(enroll + 1);
	// 	};


		
	// };


	return(
		
				<Card className="p-3 mb-3">
					<Card.Body>
						<Card.Title className="fw-bold">{name}</Card.Title>

						<Card.Subtitle>Course Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Course Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>						
					</Card.Body>
				</Card>
		
	)

};







