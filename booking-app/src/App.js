import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Error from './pages/Error';
import Logout from './pages/Logout';
import Login from './pages/Login';
import Register from './pages/Register';
import Courses from './pages/Courses';
import Home from './pages/Home';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

	//state hook for the user state that defined here for a global scope
	//initialized as an object with properties from the localStorage
	const [user, setUser] = useState({
		 id: null,
		 isAdmin: null

		// email: localStorage.getItem('email') >> changed because we already have access token
	});

	const unsetUser = () => {
		localStorage.clear();
	};


//fetch to fix loading/refreshing
	useEffect(() => {
		fetch('http://localhost:4000/users/getUserDetails', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//capture data of whoever is logged in
			console.log(data) //res: user details

			if(typeof data._id !== "undefined"){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			} else {

				setUser({
					id: null,
					isAdmin: null
				})
			}

		})
		// console.log(user);
		// console.log(localStorage);
	}, []) 

	//[user]) RES = id:, isAdmin:(remove user otherwise will loop)



	return (
		<>
			<UserProvider value={{user, setUser, unsetUser}}>		
				<Router>
					<AppNavbar/>
					<Container>
						<Routes>
							<Route exact path="/" element={<Home/>}/>
							<Route exact path="/courses" element={<Courses/>}/>
							<Route exact path="/courseView/:courseId" element={<CourseView/>}/>
							<Route exact path="/register" element={<Register/>}/>
							<Route exact path="/login" element={<Login/>}/>
							<Route exact path="/logout" element={<Logout/>}/>
							<Route exact path="*" element={<Error/>}/>
						</Routes>
					</Container>
				</Router>
			</UserProvider>
		</>
	)
}

export default App;
