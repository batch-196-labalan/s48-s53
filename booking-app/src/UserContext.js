import React from 'react';


//Create a context object
//Context object >> data type of an object that can be used to store information that can be shared to other components within the app.
const UserContext = React.createContext();


//Provide component allows other components to consume or use the context object and supply the necessary information needed.
export const UserProvider = UserContext.Provider;

export default UserContext;


//VISUALIZE

// UserContext = {
// 	user: ' ',
// 	setUser: function(),
// 	unsetUser: function()
// };